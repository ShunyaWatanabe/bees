var app_id = ""; 
var app_secret = "";
var url = "";


chrome.runtime.onInstalled.addListener(function() {
  // ERROR! Events must be registered synchronously from the start of
  // the page.
  /*var app_id = ""; 
  var app_secret = "";

  var url = "";*/

  var promise = new Promise(function(resolve, reject) {
    var variables = {};
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", "secret.txt", true);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        { 
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                var array = allText.split(",");
                for (var i=0; i<array.length; i++){
                  var tempArray = array[i].split("=");
                  variables[tempArray[0]] = tempArray[1]; 
                }
                resolve(variables); // resolve
            }
        }
    }
    rawFile.send(null);
  });

  promise.then(function(value) {
    console.log(value); // key val pair is stored here
    app_secret=value["app_secret"];
    app_id=value["app_id"];
    console.log(app_id);
    console.log(app_secret);
    url = "https://www.facebook.com/v3.0/dialog/oauth?scope=user_friends&client_id=" + app_id + "&redirect_uri=https://hpdehlmaeempnpacdadbpmfmgkdabanp.chromiumapp.org/&state={st=shunya,ds=watanabe}";
  });

  chrome.tabs.onCreated.addListener(function(tab) {
    console.log("onCreated");
  });

  chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    console.log("onUpdated");
    console.log(changeInfo);
  });

  chrome.runtime.onMessage.addListener(function(message){
    console.log("onMessage.addListener");
    console.log(message);
    if (message.signup == true){
      chrome.identity.launchWebAuthFlow({'url': url, 'interactive': true}, function(redirect_uri){
        var code = getUrlParameter('code', decodeURIComponent(redirect_uri));
        var token = getAccessToken(code);
      });
    }
    //return {"result": "success!"};
  });

  //add a message listener from the website https://developer.chrome.com/extensions/messaging#external-webpage
});

function get_current_url(){
  chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
    console.log(tabs[0]);
    console.log(tabs[0].url);
    return tabs[0].url;
  });
}

function get_val_url(url){
  console.log("url: " + url);
  $.ajax({
      type: 'GET',
      url: "https://joinbees.herokuapp.com/get_val_url",
      contentType: 'text/plain',
      data: { url: "joinhoney.com"} ,
      xhrFields: {
        // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
        // This can be used to set the 'withCredentials' property.
        // Set the value to 'true' if you'd like to pass cookies to the server.
        // If this is enabled, your server must respond with the header
        // 'Access-Control-Allow-Credentials: true'.
        withCredentials: false
      },

      headers: {
        // Set any custom headers here.
        // If you set any non-simple headers, your server must include these
        // headers in the 'Access-Control-Allow-Headers' response header.
      },

      success: function(data) {
        console.log("success!");
        console.log(data);
        display_data(data);
      },

      error: function(error) {
        console.log("error");
        console.log(error);
      }
    });
}

function display_data(data){ 
  /*
  chrome.tabs.executeScript({
    file: "content_scripts.js",
  });*/

  /*chrome.tabs.executeScript({
    code: "document.getElementById('div').style.color='red'";
  });*/
  console.log("diplay_data executed");
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function getUrlParameter(sParam, url) {
  var sPageURL = url.split('?')[1],
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    console.log(sParameterName);
    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
};

function getAccessToken(code){
  console.log("get access token");
  $.ajax({
    type: 'GET',
    url: "https://graph.facebook.com/v3.1/oauth/access_token?client_id=" + app_id + "&redirect_uri=https://hpdehlmaeempnpacdadbpmfmgkdabanp.chromiumapp.org/&state={st=shunya,ds=watanabe}&client_secret=" + app_secret + "&code=" + code,
    contentType: 'text/plain',
    data: {} ,
    xhrFields: { withCredentials: false },
    headers: {},

    success: function(data) {
      console.log("getAccessToken success!");
      chrome.storage.local.set({"access_token": data.access_token}, function() {
        console.log('access_token is set to ' + data.access_token);
      });
      getData(data.access_token);
    },

    error: function(error) {
      console.log("error: " + error);
    }
  });
}

function getData(token){  
  console.log("get data");
  console.log("https://graph.facebook.com/debug_token?input_token=" + token + "&access_token=" + app_id + "|" + app_secret);
  $.ajax({
    type: 'GET',
    url: "https://graph.facebook.com/debug_token?input_token=" + token + "&access_token=" + app_id + "|" + app_secret,
    contentType: 'text/plain',
    data: {} ,
    xhrFields: { withCredentials: false },
    headers: {},

    success: function(data) {
      console.log("getData success!");
      console.log(data);
      var user_id = data.data.user_id;
      FBapi(user_id);
      //setElements(true);
      chrome.storage.local.set({"facebook_id": user_id}, function() {
        console.log('facebook_id is set to ' + user_id);
      });
    },

    error: function(error) {
      console.log("error: " + error);
    }
  });
}

function FBapi(id){  
  console.log("FBapi");
  $.ajax({
    type: 'GET',
    url: "https://graph.facebook.com/"+ id +"?fields=name,email,friends,picture" + "&access_token=" + app_id + "|" + app_secret,
    contentType: 'text/plain',
    data: {} ,
    xhrFields: { withCredentials: false },
    headers: {},

    success: function(data) {
      console.log("FBapi success!");
      console.log(data);
      chrome.storage.local.set({"friends": data.friends.data, "facebook_name": data.name}, function() {
        console.log('friends is set to ' + data.friends.data);
        console.log('facebook_name is set to ' + data.name);
      });
    },

    error: function(error) {
      console.log(error);
    }
  });
}

function readTextFile(variable)
{
  console.log("readTextFile: " + variable);
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", "file://chrome_extension/secret.txt", true);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                var variableArray = allText.split(",");
                for (var i=0; i<variableArray.length; i++){
                  var tempArray = variableArray[i].split("=");
                  if (tempArray[0] == variable){
                    console.log(variable + " found: " + tempArray[1]);
                    return tempArray[1]; // return the value of the variable - this is asynchronous
                  }
                }
            }
        }
    }
    rawFile.send(null);
}

