window.fbAsyncInit = function() {
  FB.init({
    appId      : '*',
    cookie     : true,
    xfbml      : true,
    version    : 'v3.0'
  });
    
  FB.AppEvents.logPageView(); 

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  // when logged in, send data to chrome extension by https://developer.chrome.com/extensions/messaging#external-webpage
    
};

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

function statusChangeCallback(response){
  if (response.status === "connected"){
    console.log("Logged in and authenticated");
    setElements(true);
    testAPI();
  } else {
    console.log("Not authenticated")
    setElements(false);
  }
}

function setElements(isLoggedIn){
  if (isLoggedIn){
    document.getElementById('fb_button').style.display = "none";
    document.getElementById('logout_button').style.display = "block";
  } else {
    document.getElementById('fb_button').style.display = "block";
    document.getElementById('logout_button').style.display = "none";
  }
}

document.addEventListener('DOMContentLoaded', function(){
  document.getElementById("logout_button").onclick = function(){
    FB.logout(function(response){
      setElements(false);
    })
  }
}, false);

function testAPI(){
  console.log("test");
  FB.api('/me?fields=name,email', function(response){
    if (response && !response.error){
      console.log(response);
    }
  })
}