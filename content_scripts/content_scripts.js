// manifest.json cannot programatically decide match patterns, so do this in here instead.
$(document).ready(function(){

  // check if user is logged in
  chrome.storage.local.get("facebook_id", function(result) {
    console.log(result);
    if (result.facebook_id != null){
      console.log("createPopUp(content_scripts.html)");
      createPopUp('content_scripts/content_scripts.html');    
      includeCSS();
      return;
    }
    console.log("createPopUp(signup.html)");
    createPopUp('content_scripts/signup.html');
    includeCSS();
  });

});
  
function createPopUp(file){
	var div = document.createElement("div");
	document.body.appendChild(div); 
	div.id = "joinbees-div";

	var xhr= new XMLHttpRequest();
	xhr.open('GET', chrome.extension.getURL(file), true);
	xhr.onreadystatechange= function() {
	    if (this.readyState!==4) return;
	    if (this.status!==200) return; // or whatever error handling you want
	    document.getElementById('joinbees-div').innerHTML= this.responseText;

      if (document.getElementById('fb_button')){
  	    document.getElementById('fb_button').onclick = function(){
  	    	console.log("fb_login button is clicked");
  			}
      }

      if (document.getElementById("signin")){
        document.getElementById("signin").onclick = function(){
          console.log("signin clicked");
          email = document.getElementById("email").value;
          password = document.getElementById("password").value;
          signin(email, password);
        };
      }

      if (document.getElementById("signup")){
        document.getElementById("signup").onclick = function(){
          console.log("signup clicked");  
          chrome.runtime.sendMessage({"signup": true}, function(response){
            console.log(response);
          });
          console.log("createPopUp(content_scripts.html)");
          createPopUp('content_scripts/content_scripts.html'); 
          /*email = document.getElementById("email").value;
          password = document.getElementById("password").value;
          signup(email, password);*/
        };
      }

      if (document.getElementById("referral-submit")){
        document.getElementById("referral-submit").onclick = function(){
          console.log("referral-submit clicked");  
          url = document.getElementById("referral-url").value;

          var promise = new Promise(function(resolve, reject) {
            chrome.storage.local.get(['facebook_name', 'facebook_id'], function(result) {
              resolve(result); // resolve
            });
          });

          promise.then(function(value) {
            console.log(value);
            submit_referral(url, value["facebook_id"], value["facebook_name"]);
          });
          
          chrome.storage.local.set({"email": data, [window.location.hostname]: true}, function() {
            console.log('Value is set to ' + data);
          });
        };
      } 

      if (document.getElementById("close")){
        document.getElementById("close").onclick = function(){
          console.log("close clicked");  
          document.getElementById("joinbees-div").style.display = "none";
        };
      }

      if (document.getElementById("logout")){
        document.getElementById("logout").onclick = function(){
          console.log("logout clicked");  
          chrome.storage.local.set({"facebook_id": null, "facebook_name": null}, function() {
            console.log('facebook_id is set to ' + "null");
            console.log("createPopUp(signup.html)");
            createPopUp('content_scripts/signup.html');
          });
        };
      }

		get_val_url(window.location.hostname); // http/https and directory(?) are ignored
	}
	xhr.send();
}

function includeCSS(){
	var cssId = 'myCss';  // you could encode the css path itself to generate id..
	if (!document.getElementById(cssId))
	{
	    var head  = document.getElementsByTagName('head')[0];
	    var link  = document.createElement('link');
	    link.id   = cssId;
	    link.rel  = 'stylesheet';
	    link.type = 'text/css';
	    link.href = chrome.extension.getURL('content_scripts/content_scripts.css');
	    link.media = 'all';
	    head.appendChild(link);
	}
}

function get_val_url(url){
  console.log("url: " + url);

  var promise = new Promise(function(resolve, reject) {
    chrome.storage.local.get(['friends'], function(result) {
      console.log('friends currently is ' + result.friends);
      resolve(result.friends); // resolve
    });
  });

  promise.then(function(friends) {
    console.log(friends); // key val pair is stored here
    var friendsArr = []; // convert obj to arr containing ids
    for (var i=0; i<friends.length; i++){
      friendsArr.push(friends[i].id);
    }

    $.ajax({
      // The 'type' property sets the HTTP method.
      // A value of 'PUT' or 'DELETE' will trigger a preflight request.
      type: 'GET',

      // The URL to make the request to.
      url: "https://joinbees.herokuapp.com/get_val_url",

      // The 'contentType' property sets the 'Content-Type' header.
      // The JQuery default for this property is
      // 'application/x-www-form-urlencoded; charset=UTF-8', which does not trigger
      // a preflight. If you set this value to anything other than
      // application/x-www-form-urlencoded, multipart/form-data, or text/plain,
      // you will trigger a preflight request.
      contentType: 'text/plain',

      data: { "url": "https://" + url + "/", "friends": friendsArr.toString() } ,

      xhrFields: {
        // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
        // This can be used to set the 'withCredentials' property.
        // Set the value to 'true' if you'd like to pass cookies to the server.
        // If this is enabled, your server must respond with the header
        // 'Access-Control-Allow-Credentials: true'.
        withCredentials: false
      },

      headers: {
        // Set any custom headers here.
        // If you set any non-simple headers, your server must include these
        // headers in the 'Access-Control-Allow-Headers' response header.
      },

      success: function(data) {
        // Here's where you handle a successful response.
        console.log("success!");
        console.log(data);
        for (var i=0; i<data.length; i++){
          if (document.getElementById("referral")) {
            getPicture(data[i].facebook_id);
            document.getElementById("referral").innerHTML += "<a href=" + data[0].ref_url + ">" + data[0].ref_url + "</a>";
            document.getElementById("referral").innerHTML += "<br/>";
          }
        }
      },

      error: function(error) {
        console.log("error: " + error);
        // Here's where you handle an error response.
        // Note that if the error was due to a CORS issue,
        // this function will still fire, but there won't be any additional
        // information about the error.
      }
    });
  });
}

function submit_referral(url, facebook_id, facebook_name){
  console.log("submit_referral: " + url );
  $.ajax({
    type: 'GET',
    url: "https://joinbees.herokuapp.com/submit_referral",
    contentType: 'text/plain',
    data: { "url": url, "facebook_id": facebook_id, "facebook_name": facebook_name} ,
    xhrFields: { withCredentials: false },
    headers: {},
    success: function(data) {
      console.log("success! data: " + data);
    },
    error: function(error) {
      console.log("error: " + error);
    }
  });
}

function getPicture(id){
  console.log("getPicture");
  document.getElementById("referral").innerHTML += '<img src="https://graph.facebook.com/'+ id +'/picture?type=small">';
}