window.addEventListener('DOMContentLoaded', function(){

  chrome.storage.local.get("facebook_id", function(result) {
    console.log("get facebook_id");
    console.log(result.facebook_id);
    if (result.facebook_id != null){
      setElements(true);
    } else {
      setElements(false);
    }
  });

  document.getElementById("signup").onclick = function(){
    chrome.runtime.sendMessage({"signup": true}, function(response){
      console.log(response);
    });
    /*chrome.identity.launchWebAuthFlow({'url': url, 'interactive': true}, function(redirect_uri){          
      var code = getUrlParameter('code', decodeURIComponent(redirect_uri));
      var token = getAccessToken(code);
    });*/
  }

  document.getElementById("logout").onclick = function(){
    chrome.storage.local.set({"facebook_id": null}, function() {
      console.log("logged out");      
      setElements(false);
    });
  }
});

function setElements(isLoggedIn){
  console.log("setElements");
  if (isLoggedIn == true){
    document.getElementById("signup").style.display = "none";
    document.getElementById("logout").style.display = "block";
  }
  else if (isLoggedIn == false){
    document.getElementById("signup").style.display = "block";
    document.getElementById("logout").style.display = "none";
  }
}
